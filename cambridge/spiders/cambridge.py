import scrapy
from cambridge.items import Word

class CambridgeSpider(scrapy.Spider):
    name = "cambridge"

    def start_requests(self):
        urls = [
            "http://dictionary.cambridge.org/grammar/british-grammar/a-bit",
            "http://dictionary.cambridge.org/grammar/british-grammar/a-an-and-the",
            "http://dictionary.cambridge.org/grammar/british-grammar/abbreviations-initials-and-acronyms",
            "http://dictionary.cambridge.org/grammar/british-grammar/future/future-perfect-continuous-i-will-have-beenworking-here-ten-years"
        ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_definition)

    def parse_definition(self, response):
        word = Word()
        title = response.url.split('/')[-1].replace('-', ' ')
        definition = response.css('.content').extract_first()
        word['title'] = title
        word['definition'] = definition
        yield word
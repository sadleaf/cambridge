# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


class CambridgePipeline(object):
    def __init__(self):
        self.words = []

    def process_item(self, item, spider):
        self.words.append(item)
        return item

    def close_spider(self, spider):
        filename = "word.txt"
        with open(filename, 'w') as f:
            for word in self.words:
                f.write("%s{$$}" % word['title'])
                f.write("<font size=10>%s</font>" % word['title'])
                f.write("%s\n" % word['definition'])
                f.write("{$$}</>{$$}")

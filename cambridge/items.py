# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class Word(scrapy.Item):
    title = scrapy.Field()
    definition = scrapy.Field()
